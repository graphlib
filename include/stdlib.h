#ifndef _STDLIB
#define _STDLIB

extern void *calloc(size_t, size_t);
extern void free(void *) ;
extern void *malloc(size_t);
extern void *realloc(void *, size_t) ;

#endif
